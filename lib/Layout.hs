module Layout where

import Preferences
import ShowText
import XMonad
import XMonad.Actions.MouseResize
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ScreenCorners
import XMonad.Layout.ButtonDecoration
import XMonad.Layout.DecorationAddons
import XMonad.Layout.DwmStyle
import XMonad.Layout.GridVariants
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows
import XMonad.Layout.Minimize
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.SimpleDecoration
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import qualified XMonad.Layout.ToggleLayouts as T
import XMonad.Layout.WindowArranger
import XMonad.Layout.WindowSwitcherDecoration
import XMonad.Layout.ShowWName
import XMonad.Layout.MagicFocus

------------------------------------------------------------------------
-- space between windows
------------------------------------------------------------------------
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border 0 10 10 10) True (Border 10 10 10 10) True

------------------------------------------------------------------------
-- window bar
------------------------------------------------------------------------

myTheme :: Theme
myTheme =
  def
    { fontName = "xft:" ++ myFont ++ ":pixelsize=14",
      decoHeight = 20,
      activeColor = "#643FFF",
      inactiveColor = "#262626",
      activeBorderColor = "#643FFF",
      inactiveBorderColor = "#bac2de",
      urgentBorderColor = "#586e75",
      activeTextColor = "#CEFFAC",
      inactiveTextColor = "#cba6f7",
      urgentTextColor = "#dc322f"
    }
------------------------------------------------------------------------
-- workspace name
------------------------------------------------------------------------
myShowWNameTheme :: SWNConfig
myShowWNameTheme =
  def
    { swn_font = "xft:Agave:bold:size=60",
      swn_fade = 1.0,
      swn_bgcolor = "#232634",
      swn_color = "#cdd6f4"
    }
------------------------------------------------------------------------
-- layout hook
------------------------------------------------------------------------
myLayout = showWName' myShowWNameTheme myLayout'

myLayout' =
  screenCornerLayoutHook $
    avoidStruts $
      mouseResize $
        windowArrange $
          T.toggleLayouts full $
            mkToggle (NBFULL ?? NOBORDERS ?? MIRROR ?? EOT) myDefaultLayout
  where
    myDefaultLayout =
      grid
        ||| full
--      ||| magicFocus'
------------------------------------------------------------------------
-- layouts
------------------------------------------------------------------------
grid =
  renamed [Replace " <fc=#b7bdf8><fn=2> \61449 </fn>Grid</fc>"] $
      windowSwitcherDecoration shrinkText myTheme $
          subLayout [] (smartBorders Simplest) $
            limitWindows 12 $
              mySpacing 5 $
                mkToggle (single MIRROR) $
                  Grid (16 / 10)

full =
  renamed
    [Replace " <fc=#f38ba8><fn=2> \62556 </fn>Full</fc>"]
    Full

magicFocus' =
  renamed
    [Replace " <fc=#f38ba8><fn=2> \62556 </fn>Focus</fc>"] $
     magicFocus (Tall 1 (3/100) (1/2))

