.PHONY: build
build:
	./build/all
.PHONY: xmd
xmd:	
	./build/xmd
.PHONY: xmb
xmb:	
	./build/xmb
.PHONY: xmc
xmc:	
	./build/xmd-ctl
.PHONY: fmt
fmt:	
	./build/fmt
.DELETE_ON_ERROR:


